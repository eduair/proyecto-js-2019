app.config( function($stateProvider, $urlRouterProvider) {
  var login = {
    name: 'login',
    url: '/',
    templateUrl: 'app/components/session/views/indexView.html',
    controller: 'LoginCtrl'
  }

  var register = {
    name: 'register',
    url: '/register',
    templateUrl: 'app/components/register/views/indexView.html',
    controller: 'registerCtrl'
  }

  var albums = {
    name: 'albums',
    url: '/usuario/albums/all',
    templateUrl: 'app/components/dashboard/views/indexView.html',
    controller: 'dashboardCtrl'
  }


var singlealbum = {
    name: 'singlealbum',
    url: '/usuario/singlealbum/:id',
    templateUrl: 'app/components/single-album/views/indexView.html',
    controller: 'singlealbumCtrl'
  }

  var newalbum = {
    name: 'newalbum',
    url: '/usuario/newalbum',
    templateUrl: 'app/components/new-album/views/indexView.html',
    controller: 'newalbumCtrl'
  }

  var editalbum = {
    name: 'editalbum',
    url: '/usuario/singlealbum/edit/:id',
    templateUrl: 'app/components/edit-album/views/indexView.html',
    controller: 'editalbumCtrl'
  }

   var logout = {
    name: 'logout',
    url: '/usuario/logout',
    templateUrl: 'app/components/logout/views/indexView.html',
    controller: 'logoutCtrl'
  }

  $stateProvider.state(login);
  $stateProvider.state(register);
  $stateProvider.state(albums);
  $stateProvider.state(singlealbum);
  $stateProvider.state(newalbum);
  $stateProvider.state(editalbum);
  $stateProvider.state(logout);
  $urlRouterProvider.otherwise('/');

});