app.factory('EditAlbumService', function ($http,$window,$stateParams,$state){
	var service = {
		savenewalbum: function (cb, newalbum){
			var token = $window.localStorage.getItem('token');
			var id 		= angular.copy($stateParams.id);
			var ml = "/albumes/user/update";
			$http.post(API + ml, newalbum).then( function (res){
				var rx = res.data;
				cb(rx);
			});
		},
		getonealbum: function (cb){
			var token = $window.localStorage.getItem('token');
			var id 		= angular.copy($stateParams.id);
			var ml = "/albumes/user/"+token+"/id/"+id;
			$http.get(API + ml).then( function (res){
				var rx = res.data;
				console.log(rx);
				cb(rx);
			});
		},
	};
	return service;
});


app.controller('editalbumCtrl', ['$scope','$rootScope','$state','$stateParams','$http','TokenService','$window','$sce','EditAlbumService', function($scope, $rootScope, $state, $stateParams, $http, TokenService,$window,$sce,EditAlbumService){

TokenService.tokencheck(function (res){
	EditAlbumService.getonealbum(function (res){
		$scope.album = res[0];
		if (!$scope.album) {
			if(typeof audio !== "undefined" && audio) {
				audio.pause();
			}
			if (typeof intervaloreproducction !== "undefined" && intervaloreproducction){
				clearInterval(intervaloreproducction);
			}
			$state.go('albums');
		}	
		var opcionestipo = ['Music','Audio Book','Podcast'];
		$scope.options = opcionestipo;
		$scope.mySelect = opcionestipo[0]; 
			// albums array objectos tracks, como otro objecto.
		var checklenght;
		var newlength;
		var comparision;
		$scope.comparision = false;
		$scope.tracks = $scope.album.tracks;
		//$scope.album.track.nombre = '';
		var nombre;
		$scope.newfunct = 'Hola';
		//$scope.imagesample={'width':'250px';'height':'250px'}
		//$scope.album.img = "assets/images/albums/noalbum.jpg";
		$scope.upload = function (){
			document.getElementById("clickfile").click();
			//$scope.imagesample={'display':'none'}
		}

		$scope.uploadaudio = function (){
			nombre = angular.copy($scope.album.track.nombre);
			var alltracks = $scope.album.tracks;
			$scope.album.track.base64 = '';
			$scope.lastname = nombre;
			if (!nombre) {
				return alert('Please give a name to your track');
			}
			document.getElementById("clickmp3").click();
			checklenght = angular.copy($scope.tracks.length);
			//$scope.imagesample={'display':'none'}
		}

		function readableDuration(seconds) {
			    sec = Math.floor( seconds );    
			    min = Math.floor( sec / 60 );
			    min = min >= 10 ? min : '0' + min;    
			    sec = Math.floor( sec % 60 );
			    sec = sec >= 10 ? sec : '0' + sec;    
			    return min + ':' + sec;
		}

		$scope.trustfile = function(url) {
				var audio = document.getElementById("audio1");
				newlength = $scope.tracks.length;
				$scope.comparision = newlength == checklenght;
				comparision = angular.copy($scope.comparision);
				if (comparision == true) {
				audio.oncanplay = function() {
				if ($scope.album.track.base64 != ''){
					audio.pause();
					var duration = audio.duration;
					var duration = readableDuration(duration);
					var mp3 = angular.copy($scope.album.track.base64);
					var newtrack ={
							"name":nombre,
							"duration":duration,
							"mp3":mp3,
							"playtimes" : 0
						};
					$scope.tracks.push(newtrack);
					$scope.album.track.base64 = '';
					$scope.$apply();
					//$scope.tracks.push(newtrack);
					}
				}
		      	return $sce.trustAsUrl(url);
		      }
		};
		$scope.delete = function(index) {
		  	$scope.tracks.splice(index, 1);
		  	
		}	
		$scope.save = function() {
			var tracks = angular.copy($scope.tracks);
			var token = $window.localStorage.getItem('token');
			var name = angular.copy($scope.album.name);
			if (!name) {
			 	return alert('Please give a name to your album');
			}
			if (tracks.length == 0) {
			 	return alert('No tracks added');
			}
			var artist = angular.copy($scope.album.artist);
			if (!artist) {
				artist = 'Unkown';
			}
			var img = angular.copy($scope.album.img);
			var type   = angular.copy($scope.mySelect)
			var id = angular.copy($scope.album.id);
			var newalbum = {
			    "user": 1,
			    "userToken": token,
			    "name": name,
			    "artist": artist,
			    "img": img,
			    "tracks": tracks,
			    "type": type,
			    "id": id
			}

			EditAlbumService.savenewalbum(function (res){
				console.log(res);
				if (res) {
					alert('Album Updated');
					$state.go('albums');
				}
			}, newalbum);
		}
		});
	});
}]);