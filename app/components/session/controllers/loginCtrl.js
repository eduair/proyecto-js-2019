app.factory('LoginService', function ($http){
	var service = {
		loginUser: function (cb, loginuser){
			var ml = "/user/login";
			$http.post(API + ml, loginuser).then( function (res){
				var rx = res.data;
				cb(rx);
			});
		},
	};
	return service;
});

app.controller('LoginCtrl', ['$scope','$rootScope','$state','$stateParams','$http','LoginService','$window', function($scope, $rootScope, $state, $stateParams, $http, LoginService,$window){
	console.log("LoginCtrl :: ");

	$scope.userLogin = {};
	$scope.error 	 = "";
	$scope.invalid = {};

	$scope.login = function (){
		$scope.invalid = {'display':'none'};
		$scope.error 	= "";
		var user 		=  angular.copy( $scope.userLogin );
		var login = {
			"usuario":user.user,
			"clave":user.psw
		}
		if (!user.user) {
			$scope.invalid.user={'display':'block'}
		}
		if (!user.psw) {
			$scope.invalid.psw={'display':'block'}
		}
	
		if (!user.user || !user.psw) {
			return false;
		}
		LoginService.loginUser(function (res){
			console.log(res);
			var error = res.error;
			var success = res.success;
			if (error) {
				$scope.error = error;
			 	document.getElementById("errorlogin").click();
			} else {
				var token = res.token;
				$window.localStorage.setItem('token',token);
				$state.go('albums');
			}
			console.log("res :", res);
		}, login);
	};

}]);