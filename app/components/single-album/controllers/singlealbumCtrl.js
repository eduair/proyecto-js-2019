app.factory('SingleAlbumService', function ($http,$window,$stateParams,$state){
	var service = {
		getonealbum: function (cb){
			var token = $window.localStorage.getItem('token');
			var id 		= angular.copy($stateParams.id);
			var ml = "/albumes/user/"+token+"/id/"+id;
			$http.get(API + ml).then( function (res){
				var rx = res.data;
				console.log(rx);
				cb(rx);
			});
		},
		savenewalbum: function (cb, newalbum){
			var token = $window.localStorage.getItem('token');
			var id 		= angular.copy($stateParams.id);
			var ml = "/albumes/user/update";
			$http.post(API + ml, newalbum).then( function (res){
				var rx = res.data;
				cb(rx);
			});
		},
	};
	return service;
});

var url = "http://localhost/proyecto-js-2019/layer-api-php/albumes/user/ABC-71486/id/ALB-ABA-84947"
app.controller('singlealbumCtrl', ['$scope','$rootScope','$state','$stateParams','$http','TokenService','$window','SingleAlbumService', function($scope, $rootScope, $state, $stateParams, $http,TokenService,$window,SingleAlbumService){
	console.log("Usuarios ::");
	$scope.album = {};
	$scope.baseaudio = 'data:audio/mpeg;base64,';
	var album;
	var token = $window.localStorage.getItem('token');
	if (!token) {
		$state.go('login');
	}
	TokenService.tokencheck(function (res){
		console.log(res);
	SingleAlbumService.getonealbum(function (res){
			$scope.album = res[0];
			if (!$scope.album) {
				if(typeof audio !== "undefined" && audio) {
					audio.pause();
				}
				if (typeof intervaloreproducction !== "undefined" && intervaloreproducction){
					clearInterval(intervaloreproducction);
				}
				$state.go('albums');
			}
			album = $scope.album;
			function getRandomInt(length) {
			  length = Math.floor(length);
			  return Math.floor(Math.random() * length); //The maximum is exclusive and the minimum is inclusive
			}

		$scope.album.nombre = album.nombre;	
		$scope.album.artist = album.artist;	
		$scope.album.type = album.type;
		$scope.album.img = album.img; 
		$scope.playperc = 0;
		var intervaloreproducction;
		function getposition (currenttime,duration) {
		    var playerlength = window.getComputedStyle(document.getElementById('playertimelength')).inlineSize;
		    var lengthdurationobject = playerlength;
		    lengthdurationobject = lengthdurationobject.split("px");
		    lengthdurationobject = lengthdurationobject[0];
		    var result = (currenttime * lengthdurationobject)/duration;
		    var result = result; 
		    return result + 'px';
		}

		function newcurrenttime (position,duration) {
			var playerlength = window.getComputedStyle(document.getElementById('playertimelength')).inlineSize;
			var lengthdurationobject = playerlength;
		    lengthdurationobject = lengthdurationobject.split("px");
		    lengthdurationobject = lengthdurationobject[0];
		    var positioncurrentnew = position*duration / lengthdurationobject;
		    return positioncurrentnew;
		}

		document.getElementById('playertimelength').addEventListener('click', printPosition)	

		function getClickPosition(e) {
		  var rect = e.target.getBoundingClientRect();
		  var x = e.clientX - rect.left;
		  var y = e.clientY - rect.top;
		  return {
		    x,
		    y
		  }
		}

		function printPosition(e) {
			//var audio = document.getElementById("audiomusic");
			var duration = audio.duration;
			 var position = getClickPosition(e);
			 var result = position.x;
			 var newposition = result + 'px';
			 $scope.tracksel.position = newposition;
			 $scope.isactive = "mejs-button mejs-playpause-button mejs-play";
			$scope.isplay = "Yes";
			 $scope.$apply();
			 var clicktime = newcurrenttime(result,duration);
			// var audio = document.getElementById("audiomusic").currentTime = clicktime;
			audio.currentTime = clicktime;
			audio.pause();
			clearInterval(intervaloreproducction);

		}

		function readableDuration(seconds) {
			    sec = Math.floor( seconds );    
			    min = Math.floor( sec / 60 );
			    min = min >= 10 ? min : '0' + min;    
			    sec = Math.floor( sec % 60 );
			    sec = sec >= 10 ? sec : '0' + sec;    
			    return min + ':' + sec;
		}
				var audio = new Audio(); 
				var intervaloreproducction;
				//var duration = audio.duration;
				$scope.resultrandom = "Random Play Off";
				$scope.resultautoplay = "Auto Play Off";
				$scope.randomactive = 'No';
				$scope.done = 'No';
				$scope.tracks = album.tracks;
				$scope.tracksel = $scope.tracks[0];
				audio.src = $scope.baseaudio + $scope.tracksel.mp3;
				$scope.tracksel.duration = readableDuration(audio.duration);
				$scope.tracksel.idnext = 0;
				var tracklenght = album.tracks.length;
				var aleatorio = getRandomInt(length) 
				$scope.isactive = "mejs-button mejs-playpause-button mejs-play";
				$scope.isplay = "Yes";
				// albums array objectos tracks, como otro objecto.
				if($stateParams.id){
					var id 		= angular.copy($stateParams.id);

					if(!find){
						$state.go('albums');
					}
				}

			//var audio = document.getElementById("audiomusic");
			audio.oncanplay = function() {
			var currentime = readableDuration(audio.currentTime);
			var duration = readableDuration(audio.duration);
			$scope.duracionaudio = duration;
			$scope.tracksel.duration = duration;
			$scope.currentime = currentime;
			var intervaloreproducction;
			var duration = readableDuration(audio.duration);
			$scope.tracksel.duration = duration;
			$scope.$apply();
			};
			function myTimer() {
				//var audio = document.getElementById("audiomusic");
				var currentime = readableDuration(audio.currentTime);
				var duration = readableDuration(audio.duration);
				if (audio.currentTime == audio.duration) {
					console.log($scope.album);
					if ($scope.resultautoplay == "Auto Play Off") {
						$scope.playperc = 0;
						$scope.done = 'No';
						$scope.playaudio();
						$scope.currentime = "00:00";

					} else {
						//$scope.album.tracks = $scope.tracks;
						//console.log($scope.album);
						//clearInterval(intervaloreproducction);
						var idnext = angular.copy($scope.tracksel.idnext); 
						$scope.adelante(idnext,'loop');
						//audio = document.getElementById("audiomusic");
						//audio = new Audio();
						audio.load();
						audio.src = $scope.baseaudio + $scope.tracksel.mp3;
						audio.play();
					}
				}
				var positionpunt = getposition(audio.currentTime,audio.duration);
				$scope.tracksel.position = positionpunt;
				$scope.duracionaudio = duration;
				$scope.currentime = currentime;
				$scope.$apply();
				if ($stateParams.id != id) {
					audio.pause();
					audio.currentTime = 0;
					clearInterval(intervaloreproducction);
				}
				var asumar = (50*100)/(audio.duration*1000);
				if (asumar) {
					$scope.playperc = $scope.playperc + asumar;
				} else {
					$scope.playperc = $scope.playperc + 0;
				}
				if ($scope.playperc > 1 && $scope.done == 'No') {
					var checkbefore = angular.copy($scope.tracksel.playtimes);
					$scope.tracksel.playtimes = checkbefore + 1;
					var position = angular.copy($scope.tracks.indexOf($scope.tracksel));
					var playtimes = angular.copy($scope.tracksel.playtimes);
					$scope.album.tracks[position].playtimes = angular.copy(playtimes);
					$scope.done = 'Yes';
					var newalbum = angular.copy($scope.album);
					SingleAlbumService.savenewalbum(function (res){
					}, newalbum);
				}
			 }
			$scope.playaudio = function (){
				//var audio = document.getElementById("audiomusic");
				//var audio = document.getElementById("audiomusic");
				var duration = readableDuration(audio.duration);
				$scope.tracksel.duration = duration;
				var currentime = readableDuration(audio.currentTime);
				$scope.duracionaudio = duration;
				$scope.currentime = currentime;
				if ($scope.isplay == "Yes"){ 
					$scope.isactive = "mejs-button mejs-playpause-button mejs-pause";
					$scope.isplay = "No";
					audio.play();
					intervaloreproducction = setInterval(myTimer, 50);
				} else if ($scope.isplay =="No") {
					$scope.isactive = "mejs-button mejs-playpause-button mejs-play";
					$scope.isplay = "Yes";
					$scope.currentime = '00:00';
					//$scope.tracksel.position = '5px';
					clearInterval(intervaloreproducction);
					audio.pause();
				}
			}

			$scope.reproducir = function (id,select){
				$scope.playperc = 0;
				if (intervaloreproducction){
					$scope.done = 'No';
					clearInterval(intervaloreproducction);
					audio.currentTime = 0;
					$scope.tracksel.position = '0px';
				}
				$scope.tracksel = $scope.tracks[id];
				audio.src = angular.copy($scope.baseaudio + $scope.tracksel.mp3);
				audio.load();
				$scope.tracksel.idnext = id;
				$scope.isactive = "mejs-button mejs-playpause-button mejs-pause";
				$scope.isplay = "No";
				audio.play();
				intervaloreproducction = setInterval(myTimer, 50);
				if (select) {
					$scope.isactive = "mejs-button mejs-playpause-button mejs-play";
					$scope.isplay = "Yes";
					$scope.currentime = "00:00";
					audio.pause();
					$scope.done = 'No';
					clearInterval(intervaloreproducction);
				}
			}

			$scope.adelante = function (id,loop,random){
					var lenghtscopetracks =  angular.copy($scope.tracks.length);
					var maximasposiciones = lenghtscopetracks - 1;
					if (random || $scope.randomactive == 'Yes'){
						var newid = getRandomInt(lenghtscopetracks);
					} else {
						if (id < maximasposiciones ){
							var newid = id + 1;
						} else {
							var newid = 0;
						}
					}
					$scope.playperc = 0;
					$scope.done = 'No';
					$scope.tracksel = $scope.tracks[newid];
					$scope.tracksel.idnext  = newid;
					$scope.reproducir(newid);
					$scope.tracksel.duration = $scope.tracksel['duration'];
					audio.oncanplay = function() {
					$scope.tracksel.duration = readableDuration(audio.duration);
					};
					//$scope.playaudio;	
				if (!loop) {
					$scope.isactive = "mejs-button mejs-playpause-button mejs-play";
					$scope.isplay = "Yes";
					$scope.currentime = "00:00";
					clearInterval(intervaloreproducction);
					audio.pause();
				}
			}

			$scope.atras = function (){
				var lenghtscopetracks =  angular.copy($scope.tracks.length);
				var id = angular.copy($scope.tracks.indexOf($scope.tracksel));
				var maximasposiciones = lenghtscopetracks - 1;
				if (id > 0){
					var newid = id - 1;
				} else {
					var newid = maximasposiciones;
				}
				$scope.tracksel = $scope.tracks[newid];
				$scope.done = 'No';
				$scope.currentime = '00:00';
				$scope.tracksel.position = '5px';
				$scope.tracksel.idnext  = newid;
				$scope.reproducir(newid);
				clearInterval(intervaloreproducction);
				$scope.playperc = 0;
				$scope.isactive = "mejs-button mejs-playpause-button mejs-play";
				$scope.isplay = "Yes";
				$scope.tracksel.duration = $scope.tracksel['duration'];
				audio.oncanplay = function() {
					$scope.tracksel.duration = readableDuration(audio.duration);
				};
				//intervaloreproducction = setInterval(myTimer, 50);
				audio.pause();

			}

			$scope.aleatorio = function() {
				if ($scope.randomactive == 'No') {
				// var lenghtscopetracks = angular.copy($scope.tracks.length)
				// var newid = getRandomInt(lenghtscopetracks);
				// $scope.adelante(newid,'loop','random');
				// var newid2 = getRandomInt(lenghtscopetracks);
				$scope.randomactive = 'Yes';
				$scope.resultrandom = "Random Play On";
				// $scope.tracksel.idnext = newid2;
				} else {
					$scope.randomactive = 'No';
					$scope.resultrandom = "Random Play Off";
				}

			}

			$scope.autoplay = function() {
				if ($scope.resultautoplay == "Auto Play Off") {
					$scope.resultautoplay = "Auto Play On";
				} else if ($scope.resultautoplay == "Auto Play On") {
					$scope.resultautoplay = "Auto Play Off";
				}
			}

			$scope.goalbums = function() {
				if(typeof audio !== "undefined" && audio) {
					audio.pause();
				}
				if (typeof intervaloreproducction !== "undefined" && intervaloreproducction){
					clearInterval(intervaloreproducction);
				}

				$state.go('albums');
			}
		});
	})	
}]);