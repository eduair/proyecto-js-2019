app.factory('RegisterService', function ($http){
	var service = {
		registerUser: function (cb, newuser){
			var ml = "/user/new";
			$http.post(API + ml, newuser).then( function (res){
				var rx = res.data;
				cb(rx);
			});
		},
	};
	return service;
});

app.controller('registerCtrl', ['$scope','$rootScope','$state','$stateParams','$http','RegisterService', function($scope, $rootScope, $state, $stateParams, $http, RegisterService){
	$scope.userRegister = {};
	$scope.invalid = {};
	var success;
	$scope.logingo = function (){
		document.getElementById("cancelmodal").click();
		$state.go('login');
	}
	$scope.register = function (){
		$scope.invalid={'display':'none'};
		$scope.error 	= "";
		var nombre = angular.copy($scope.userRegister.nombre);
		var usuario = angular.copy($scope.userRegister.usuario);
		var clave = angular.copy($scope.userRegister.clave);
		var agree = angular.copy($scope.userRegister.agree);
		if (!agree) {
			$scope.invalid.agree={'display':'block'}
		}
		if (!nombre) {
			$scope.invalid.nombre={'display':'block'}
		}
		if (!usuario) {
			$scope.invalid.usuario={'display':'block'}
		}
		if (!clave) {
			$scope.invalid.clave={'display':'block'}
		}

		if (!nombre || !usuario  || !clave || !agree) {
			return false;
		}

		$scope.invalid={'display':'none'};
		//$scope.invalid = {'display':'block'}
		var newuser = {
			    "nombre": nombre,
			    "usuario": usuario,
			    "clave": clave,
			    "activo": true
			}

		RegisterService.registerUser(function (res){
			var success = res.success;
			if (success) {
				document.getElementById("successregister").click();
			}
			console.log("res :", res);
		}, newuser);


	};

}]);