app.factory('DashboardService', function ($http,$window){
	var service = {
		allalbums: function (cb){
			var token = $window.localStorage.getItem('token');
			var ml = "/albumes/user/"+token+"/all";
			console.log(API + ml);
			$http.get(API + ml).then( function (res){
				var rx = res.data;
				cb(rx);
			});
		},
	};
	return service;
});

app.controller('dashboardCtrl', ['$scope','$rootScope','$state','$stateParams','$http','TokenService','$window','DashboardService', function($scope, $rootScope, $state, $stateParams, $http, TokenService,$window, DashboardService){
	var token = $window.localStorage.getItem('token');
	$scope.welcome = '';
	if (!token) {
		$state.go('login');
	}
	TokenService.tokencheck(function (res){
		DashboardService.allalbums(function (res){
			$scope.albums = res;
			if (res.length == 0) {
				$scope.welcome = 'Welcome to YourMusic, you can start uploading your albums by clicking here';
			}
		});
	});	
}]);