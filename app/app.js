var app = angular.module('app',['ui.router', 'naif.base64']);
var API = "http://localhost/proyecto-js-2019/api-php";

app.factory('TokenService', function ($http,$window,$state,$stateParams){
	var service = {
		tokencheck: function (cb){
			var ml = "/user/token/";
			var token = $window.localStorage.getItem('token');
			$http.get(API + ml + token).then( function (res){
				var rx = res.data;
				if (!rx || rx == '' || typeof rx == "undefined") {
					$state.go('login');
				}
				cb(rx);
			});
		},
		logoutService: function (){
			$window.localStorage.removeItem('token');
			$state.go('login');
		},
	};
	return service;
});

app.controller('MainCtrl', ['$scope','$rootScope','TokenService', function ($scope, $rootScope, TokenService){
	console.log("MainCtrl :: ");

	$scope.titulo 		= "Main Controller";
	$scope.usuario;

	$rootScope.state 	= "login";

	$scope.enviarDatos 	= function (){
		var coso 		= $scope.usuario;
		console.log(coso);
	};

}]);

console.log(app);