<?php 
	// INCLUDE CLASS
	require 'classes/Album.php';

	$objAlbum = new Album();
	
	$params 	= json_decode(file_get_contents('php://input'), true);
	$name 		= $match['name'];

	if($params){
		// GET USER BY ID
		if($name == 'album-new'){
			$response = $objAlbum->insertNewAlbum($params);
			echo json_encode($response);
		}
		if($name == 'album-update'){
			$response = $objAlbum->updateAlbum($params);
			echo json_encode($response);
		}
	} else {
		echo json_encode( array("response" => 'err') );
	}
?>