<?php 

/**
 * 
 */
class User
{

	private $model = "models/users";
	
	public function getUserByToken($user){
		$result = json_decode( file_get_contents( $this->model . "/users.json") );
		$res 	= array();

		foreach ($result as $key => $value) {
			if($user == $value->token){
				$res[] = $value;
			}
		}

		unset($res[0]->clave);

		return $res[0];
	}

	public function login($user){
		$result = json_decode( file_get_contents( $this->model . "/users.json") );
		$res 	= array();
		foreach ($result as $key => $value) {
			if($user["usuario"] == $value->usuario && md5($user["clave"]) ==  $value->clave){
				$res[] = $value;
			}
		}
		if(!empty($res)){
			return $res[0];
		} else {
			return array("error" => "Usuario/clave incorrecto.");
		}
	}

	public function getAlbumesByUserAndId($user, $id){
		$result = json_decode( file_get_contents( $this->model . "/users.json") );
		$res 	= array();

		foreach ($result as $key => $value) {
			if($user == $value->userToken && $id == $value->id){
				$res[] = $value;
			}
		}
		return $res;
	}

	public function insertNewUser($album){
		$res 	   = json_decode( file_get_contents( $this->model . "/users.json") );
		$album["id"] 	= rand(0, 190000);
		$album["token"] = "ABC-" . $album["id"];
		$album["clave"] = md5($album["clave"]);

		array_push($res, $album);

		$r = file_put_contents( $this->model . "/users.json", json_encode($res) );
		
		if($r){
			return array("success" => true, "user" => $album);
		}
	}

	public function updateUser($album){
		$res 	 = json_decode( file_get_contents( $this->model . "/users.json") );

		if(!empty($album["clave"])){
			$album["clave"] = md5($album["clave"]);
		}

		for ($i=0; $i < count($res); $i++) { 
			$alb = $res[$i];

			if($alb->id == $album["id"]){
				if(empty($album["clave"])){
					$album["clave"] = $res[$i]->clave;
				}
				$res[$i] = $album;
			}
		}
		
		$r = file_put_contents( $this->model . "/users.json", json_encode($res) );

		if($r){
			return $res;
		}
	}
}

?>