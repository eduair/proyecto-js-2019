<?php 

/**
 * 
 */
class Album
{

	private $model = "models/albumes";
	
	public function getAlbumesByUser($user){
		$result = json_decode( file_get_contents( $this->model . "/albumes.json") );
		$res 	= array();

		foreach ($result as $key => $value) {
			if($user == $value->userToken){
				$res[] = $value;
			}
		}
		return $res;
	}

	public function getAlbumesByUserAndId($user, $id){
		$result = json_decode( file_get_contents( $this->model . "/albumes.json") );
		$res 	= array();

		foreach ($result as $key => $value) {
			if($user == $value->userToken && $id == $value->id){
				$res[] = $value;
			}
		}
		return $res;
	}

	public function insertNewAlbum($album){
		$res 	   = json_decode( file_get_contents( $this->model . "/albumes.json") );
		$album["id"] = "ALB-ABA-" . rand(0, 190000);

		array_push($res, $album);

		$r = file_put_contents( $this->model . "/albumes.json", json_encode($res) );
		
		if($r){
			return $res;
		}
	}

	public function updateAlbum($album){
		$res 	 = json_decode( file_get_contents( $this->model . "/albumes.json") );
		for ($i=0; $i < count($res); $i++) { 
			$alb = $res[$i];
			if($alb->id == $album["id"]){
				$res[$i] = $album;
			}
		}
		
		$r = file_put_contents( $this->model . "/albumes.json", json_encode($res) );

		if($r){
			return $res;
		}
	}
}

?>