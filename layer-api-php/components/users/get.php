<?php 
	require 'classes/User.php';

	$objAlbum = new User();

	$params 	= $match['params'];
	$name 		= $match['name'];
		
	// GET STUDENT BY ID
	if($name == 'user-by-token'){
		$id 		= $params["token"];
		$response 	= $objAlbum->getUserByToken($id);
		echo json_encode($response);
	}
	if($name == 'album-by-id-user'){
		$token 		= $params["token"];
		$id 		= $params["id"];
		$response 	= $objAlbum->getAlbumesByUserAndId($token, $id);
		echo json_encode($response);
	}
?>