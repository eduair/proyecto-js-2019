<?php
ini_set('display_errors', 'Off');
error_reporting(0);
header("Content-Type: application/json");
//ini_get('allow_url_fopen');

require 'classes/AltoRouter.php';
require 'config.php';


$router = new AltoRouter();
	
	if(!empty(ENV)){
		$router->setBasePath(ENV);
	}
	
	// MATCH ROUTING - DEFAULT
		$router->map('GET','/', 'components/home/index.php', 'home');

		$router->map('POST','/user/login', 'components/users/post.php', 'login');


		$router->map('GET','/albumes/user/[*:token]/all', 'components/albumes/get.php', 'album-by-user');
		$router->map('POST','/albumes/user/new', 		  'components/albumes/post.php', 'album-new');
		$router->map('POST','/albumes/user/update', 	  'components/albumes/post.php', 'album-update');
		$router->map('GET','/albumes/user/[*:token]/id/[*:id]', 'components/albumes/get.php', 'album-by-id-user');


		$router->map('GET','/user/token/[*:token]', 'components/users/get.php', 'user-by-token');
		$router->map('POST','/user/new', 		    'components/users/post.php', 'user-new');
		$router->map('POST','/user/update', 		'components/users/post.php', 'user-update');

	// match current request
	$match = $router->match();

	if($match) {
	  require $match['target'];
	} else {
	   echo json_encode( array("response" => "404") );
	}

?>